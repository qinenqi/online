package com.example.onlinees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineEsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineEsApplication.class, args);
    }

}
