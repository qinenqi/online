package com.example.onlinees.modules.rest.service;



public interface RestHighLevelClientService {

    /**
     * 新增一条记录
     */
    public void index();

    /**
     *  删除一条记录
     * @param indexName 索引
     * @param id 该记录的id
     */
    public void delete(String indexName, String id);

    /**
     * 更新
     */
    public void update();

    /**
     *  查询
     * @param indexName 索引
     * @param id 该记录的id
     */
    public void getIndex(String indexName, String id);
}
