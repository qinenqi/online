package com.example.onlinees.modules.rest.entry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class News implements Serializable {

    private String title;
    private String tag;
    private String publishTime;


}
