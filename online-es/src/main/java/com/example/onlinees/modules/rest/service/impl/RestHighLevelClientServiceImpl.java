package com.example.onlinees.modules.rest.service.impl;

import com.example.onlinees.modules.rest.service.RestHighLevelClientService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.get.GetResult;
import org.elasticsearch.script.Script;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Service
@Slf4j
public class RestHighLevelClientServiceImpl implements RestHighLevelClientService {

    @Autowired
    private RestHighLevelClient highLevelClient;

    /**
     * 新增一条记录
     * 不存钻则新增，存在则更新
     */
    public void index(){
        try{
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("user", "kimchy");
            jsonMap.put("postDate", new Date());
            jsonMap.put("message", "trying out Elasticsearch");
            IndexRequest indexRequest = new IndexRequest("posts")
                    .id("1").source(jsonMap);
            IndexResponse indexResponse = highLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            log.info("新增一条索引成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     *  删除一条记录
     * @param indexName 索引
     * @param id 该记录的id
     */
    public void delete(String indexName, String id){
        try{
            DeleteRequest deleteRequest = new DeleteRequest(indexName, id);
            DeleteResponse delete = highLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
            log.info("删除成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * 更新
     */
    public void update(){
        try{
            UpdateRequest updateRequest = new UpdateRequest("posts", "_doc", "1")
                    .script(new Script("测试数据"));
            UpdateResponse update = highLevelClient.update(updateRequest, RequestOptions.DEFAULT);
            GetResult getResult = update.getGetResult();
            log.info("更新数据成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *  查询
     * @param indexName 索引
     * @param id 该记录的id
     */
    public void getIndex(String indexName, String id){
        try{
            GetRequest getRequest = new GetRequest(indexName, id);
            GetResponse documentFields = highLevelClient.get(getRequest, RequestOptions.DEFAULT);
            String index = documentFields.getIndex();
            String esid = documentFields.getId();
            Map<String, Object> source = documentFields.getSource();
            long version = documentFields.getVersion();
            log.info("index:" + index + " _id:" + esid + " _source:" + source + " _version:" + version);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

















