package com.example.onlinees.modules.rest.service;

import com.example.onlinees.modules.rest.entry.Common;

public interface NewsService {

    /**
     * 添加一条记录
     * @param common
     */
    public void  add(Common common);

    /**
     * 批量插入数据
     */
    public void  batchAdd(Common common);
}
