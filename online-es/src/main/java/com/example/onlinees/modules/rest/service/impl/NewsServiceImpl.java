package com.example.onlinees.modules.rest.service.impl;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.example.onlinees.modules.rest.entry.Common;
import com.example.onlinees.modules.rest.entry.News;
import com.example.onlinees.modules.rest.service.NewsService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private RestHighLevelClient highLevelClient;

    /**
     * 添加一条记录
     * @param common
     */
    public void  add(Common common){
        try{
            String index = common.getIndex();
            String type = common.getType();
            IndexRequest indexRequest = new IndexRequest(index, type);

            News news = new News();
            news.setTitle("新闻标题");
            news.setTag("新闻");
            news.setPublishTime("2021-08-06");
            String source = JSONUtils.serializeObject(news);

            indexRequest.source(source, XContentType.JSON);
            highLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 批量插入数据
     */
    public void  batchAdd(Common common){
        BulkRequest bulkRequest = new BulkRequest();
        List<IndexRequest> requests = generateRequests(common);
        for (IndexRequest indexRequest : requests) {
            bulkRequest.add(indexRequest);
        }
        try {
            highLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<IndexRequest> generateRequests(Common common){
        List<IndexRequest> requests = new ArrayList<>();
        requests.add(generateNewsRequest(common, "中国下水山东舰", "军事", "2018-01-27T08:34:00Z"));
        requests.add(generateNewsRequest(common, "奥运会开始了", "体育", "2018-01-26T14:34:00Z"));
        requests.add(generateNewsRequest(common, "射击比赛获得了金牌", "体育", "2018-01-26T08:34:00Z"));
        requests.add(generateNewsRequest(common, "巴萨和梅西解约", "体育", "2018-01-26T20:34:00Z"));
        return requests;
    }

    public IndexRequest generateNewsRequest(Common common, String title, String tag, String publishTime){
        String index = common.getIndex();
        String type = common.getType();
        IndexRequest indexRequest = new IndexRequest(index, type);
        try{
            News news = new News();
            news.setTitle(title);
            news.setTag(tag);
            news.setPublishTime(publishTime);
            String source = JSONUtils.serializeObject(news);
            indexRequest.source(source, XContentType.JSON);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return indexRequest;
    }

}



