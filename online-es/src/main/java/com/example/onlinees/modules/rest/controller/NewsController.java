package com.example.onlinees.modules.rest.controller;


import com.example.onlinecommon.utils.R;
import com.example.onlinees.modules.rest.entry.Common;
import com.example.onlinees.modules.rest.service.NewsService;
import org.elasticsearch.action.index.IndexRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/newsController")
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     *  添加一条记录
     * @param common
     * @return
     */
    @RequestMapping("/add")
    public R add(@RequestBody Common common){
        newsService.add(common);
        return R.ok();
    }


    /**
     *  批量插入数据
     * @return
     */
    @RequestMapping("/batchAdd")
    public R batchAdd(@RequestBody Common common){
        newsService.batchAdd(common);
        return R.ok();
    }


}




















