package com.example.onlinees.modules.rest.controller;


import com.example.onlinecommon.utils.R;
import com.example.onlinees.modules.rest.service.RestHighLevelClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;

@RestController
@RequestMapping("/restHighLevelClientController")
public class RestHighLevelClientController {



    @Autowired
    private RestHighLevelClientService restHighLevelClientService;

    /**
     * 新增一条记录
     * @return
     * @throws IOException
     */
    @RequestMapping("/index")
    public R index(){
        restHighLevelClientService.index();
        return R.ok();
    }

    /**
     *  删除一条记录
     * @param indexName 索引
     * @param id 该记录的id
     * @return
     */
    @GetMapping("/delete")
    public R delete(@RequestParam String indexName, @RequestParam String id){
        restHighLevelClientService.delete(indexName, id);
        return R.ok();
    }

    /**
     * 修改也可以通过index存在则替换或者upsets
     * @return
     */
    @RequestMapping("/update")
    public R update(){
        restHighLevelClientService.update();
        return R.ok();
    }

    /**
     *  查询
     * @param indexName 索引
     * @param id 该记录的id
     * @return
     */
    @RequestMapping("/getIndex")
    public R getIndex(@RequestParam String indexName, @RequestParam String id){
        restHighLevelClientService.getIndex(indexName, id);
        return R.ok();
    }
}
