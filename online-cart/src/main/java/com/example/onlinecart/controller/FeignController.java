package com.example.onlinecart.controller;


import com.example.onlinecart.feign.CouponFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试远程调用
 */
@RestController
@RequestMapping("/feignController")
public class FeignController {

    @Autowired
    private CouponFeignService couponFeignService;

    /**
     * cart调用coupon的方法
     * @return
     */
    @RequestMapping("/testCoupon")
    public String testCoupon(){
        String test = couponFeignService.test();
        test = "测试远程调用 cart调用coupon的方法，得到的返回值是：" + test;
        return test;
    }
}
