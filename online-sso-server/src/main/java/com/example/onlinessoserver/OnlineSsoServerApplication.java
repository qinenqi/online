package com.example.onlinessoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineSsoServerApplication.class, args);
    }

}
