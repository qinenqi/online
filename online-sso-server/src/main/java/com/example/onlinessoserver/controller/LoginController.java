package com.example.onlinessoserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 *  单点登录值之thymeleaf渲染页面
 */
@Controller
@RequestMapping("/loginController")
public class LoginController {


    @Autowired
    private StringRedisTemplate redisTemplate;


    @GetMapping("/test")
    @ResponseBody
    public String test(){
        return "测试登录服务器";
    }

    /**
     *  根据token信息，查询数据  并判断
     * @param token
     * @return
     */
    @ResponseBody
    @GetMapping("/userInfo")
    public boolean userInfo(@RequestParam("token") String token){
        boolean flag = false;
        String userInfor = redisTemplate.opsForValue().get(token); // 从redis中获取用户信息
        String[] arr = userInfor.split("_");
        String username = arr[0];
        String password = arr[1];
        // 拿着用户名和密码查询数据库，判断此人信息是否正确
        if(!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)){
            flag = true;
        }
        return flag;
    }



    /**
     * ssoserver.com:8082/login.html?redirect_url=1111
     * @param url
     * @param model
     * @param sso_token
     * @return
     */
    @GetMapping("/login.html")
    public String loginPage(@RequestParam("redirect_url") String url, Model model,
                            @CookieValue(value = "sso_token",required = false) String sso_token){
        if(!StringUtils.isEmpty(sso_token)){
            //说明之前有人登录过，浏览器留下了痕迹
            return "redirect:"+url+"?token="+sso_token;
        }

        model.addAttribute("url",url);
        return "login";
    }

    @PostMapping("/doLogin")
    public String doLogin(@RequestParam("username") String username,
                          @RequestParam("password")String password,
                          @RequestParam("url")String url,
                          HttpServletResponse response){

        if(!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)){ // 需要进行查询数据库校验，此处简单处理
            String userInfor = username + "_" + password;
            //登录成功，跳回之前页面

            //把登录成功的用户存起来。
            String uuid = UUID.randomUUID().toString().replace("-","");
            redisTemplate.opsForValue().set(uuid,userInfor);
            Cookie sso_token = new Cookie("sso_token",uuid);
            response.addCookie(sso_token);
            return "redirect:"+url+"?token="+uuid;
        }
        //登录失败，展示登录页
        return "login";
    }
}
