package com.example.onlineshortmessage.controller;

import com.example.onlinecommon.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testController")
@Slf4j
public class TestController {


    /**
     *  es 测试 网关
     * @return
     */
    @RequestMapping("/test")
    public R test(){
        log.info("online-short-message 测试 R 成功");
        return R.ok("online-short-message 测试 R 成功");
    }
}
