package com.example.onlineshortmessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

// 如果想连接数据库，请更换为  @SpringBootApplication
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class OnlineShortMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineShortMessageApplication.class, args);
    }

}
