package com.example.onlineoss.controller;

import com.aliyun.oss.model.AccessControlList;
import com.aliyun.oss.model.BucketInfo;
import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

/**
 *  对象存储的 存储空间 控制器  Bucket
 */
@RestController
@RequestMapping("/bucketController")
public class BucketController {

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKeyId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKeySecret ;

    @Value("${spring.cloud.alicloud.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucketName;

    @Autowired
    private BucketService bucketService;


    /**
     *  创建存储空间
     * @param bucketName
     * @return
     */
    @RequestMapping("/createBucket")
    public R createBucket(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.createBucket(ossEntry);
        return R.ok();
    }

    /**
     *  列举存储空间
     * @return
     */
    @RequestMapping("/listBucket")
    public R listBucket(){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.listBucket(ossEntry);
        return R.ok();
    }

    /**
     *  判断存储空间是否存在
     * @param bucketName
     * @return
     */
    @RequestMapping("/doesBucketExist")
    public R doesBucketExist(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        boolean b = bucketService.doesBucketExist(ossEntry);
        return R.ok(String.valueOf(b));
    }

    /**
     *  获取存储空间所在的地域
     * @return
     */
    @RequestMapping("/getBucketLocation")
    public R getBucketLocation(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        String bucketLocation = bucketService.getBucketLocation(ossEntry);
        return R.ok(bucketLocation);
    }

    /**
     * 获取存储空间的信息
     * @param bucketName
     * @return
     */
    @RequestMapping("/getBucketInfo")
    public R getBucketInfo(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        BucketInfo bucketInfo = bucketService.getBucketInfo(ossEntry);
        Map<String, Object> map = new HashMap<>();
        map.put("getBucketInfo", bucketInfo);
        return R.ok(map);
    }

    /**
     *  设置存储空间的访问权限
     * @param bucketName
     * @return
     */
    @RequestMapping("/setBucketAcl")
    public R setBucketAcl(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.setBucketAcl(ossEntry);
        return R.ok();
    }

    /**
     *  获取存储空间的访问权限
     * @param bucketName
     * @return
     */
    @RequestMapping("/getBucketAcl")
    public R getBucketAcl(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        AccessControlList bucketAcl = bucketService.getBucketAcl(ossEntry);
        Map<String, Object> map = new HashMap<>();
        map.put("getBucketAcl", bucketAcl);
        return R.ok(map);
    }

    /**
     * 删除存储空间
     * @param bucketName
     * @return
     */
    @RequestMapping("/deleteBucket")
    public R deleteBucket(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.deleteBucket(ossEntry);
        return R.ok();
    }

    /**
     * 设置Bucket标签
     * @param bucketName
     * @return
     */
    @RequestMapping("/setBucketTagging")
    public R setBucketTagging(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.setBucketTagging(ossEntry);
        return R.ok();
    }

    /**
     * 获取Bucket标签信息
     * @param bucketName
     * @return
     */
    @RequestMapping("/getAllTags")
    public R getAllTags(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        Map<String, String> allTags = bucketService.getAllTags(ossEntry);
        return R.ok(allTags.toString());
    }

    /**
     * 列举带指定标签的Bucket
     * @return
     */
    @RequestMapping("/listBuckets")
    public R listBuckets(){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.listBuckets(ossEntry);
        return R.ok();
    }

    /**
     * 删除Bucket标签
     * @param bucketName
     * @return
     */
    @RequestMapping("/deleteBucketTagging")
    public R deleteBucketTagging(String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        bucketService.deleteBucketTagging(ossEntry);
        return R.ok();
    }

}
