package com.example.onlineoss.controller;

import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ObjectUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *  文件上传控制器
 */
@RequestMapping("/objectUploadController")
@RestController
public class ObjectUploadController {


    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKeyId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKeySecret ;

    @Value("${spring.cloud.alicloud.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucketName;

    @Autowired
    private ObjectUploadService objectUploadService;

    // 简单文件上传
    /**
     * 上传字符串
     * @return
     */
    @RequestMapping("/putObject")
    public R putObject(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        objectUploadService.putObject(ossEntry);
        return R.ok();
    }

    /**
     * 上传Byte数组
     * @return
     */
    @RequestMapping("/putObjectByte")
    public R putObjectByte(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        objectUploadService.putObjectByte(ossEntry);
        return R.ok();
    }

    /**
     * 上传网络流
     * @return
     */
    @RequestMapping("/putObjectUrl")
    public R putObjectUrl(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        objectUploadService.putObjectUrl(ossEntry);
        return R.ok();
    }

    /**
     * 上传文件流
     * @return
     */
    @RequestMapping("/putObjectFile")
    public R putObjectFile(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        objectUploadService.putObjectFile(ossEntry);
        return R.ok();
    }

    /**
     * 上传文件
     * @return
     */
    @RequestMapping("/putObjectFile02")
    public R putObjectFile02(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        objectUploadService.putObjectFile02(ossEntry);
        return R.ok();
    }

}
