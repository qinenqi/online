package com.example.onlineoss.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.BucketList;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.AliOssCommonSercice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

/**
 *  oss 对象存储的一些基本方法
 */
@RestController
@RequestMapping("/aliOssController")
@Slf4j
public class AliOssController {


    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKeyId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKeySecret ;

    @Value("${spring.cloud.alicloud.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucketName;

    /**
     * 阿里云 对象储存的 公共方法
     */
    @Autowired
    private AliOssCommonSercice aliOssCommonSercice;


    /**
     *  创建 存储空间
     * @param bucketName
     * @return
     */
    @RequestMapping("/createBucket")
    public R createBucket(@RequestParam String bucketName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        R bucket = aliOssCommonSercice.createBucket(ossEntry);
        return bucket;
    }

    /**
     *  上传  简单文件（字符串）
     * @param
     * @return
     */
    @RequestMapping("/uploadFile")
    public R uploadFile(){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        R r = aliOssCommonSercice.uploadFile(ossEntry);
        return r;
    }


    /**
     * 下载文件
     * @param fileName
     * @return
     */
    @RequestMapping("/downFile")
    public R downFile(@RequestParam String fileName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        R r = aliOssCommonSercice.downFile(fileName, ossEntry);
        return r;
    }


    /**
     *  列举文件
     * @return
     */
    @RequestMapping("/listObject")
    public R listObject(){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        R r = aliOssCommonSercice.listObject(ossEntry);
        return r;
    }

    /**
     *  删除文件
     * @param fileName
     * @return
     */
    @RequestMapping("/deleteFile")
    public R deleteFile(@RequestParam String fileName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName, fileName);
        R r = aliOssCommonSercice.deleteFile(ossEntry);
        return r;
    }
}
