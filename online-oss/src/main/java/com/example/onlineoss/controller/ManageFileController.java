package com.example.onlineoss.controller;

import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ManageFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理文件控制器
 */
@RestController
@RequestMapping("/manageFileController")
public class ManageFileController {

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKeyId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKeySecret ;

    @Value("${spring.cloud.alicloud.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucketName;

    @Autowired
    private ManageFileService manageFileService;

    /**
     * 判断文件是否存在
     * @param bucketName
     * @param objectName
     * @return
     */
    @RequestMapping("/doesObjectExist")
    public R doesObjectExist(String bucketName, String objectName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        ossEntry.setObjectName(objectName);
        boolean b = manageFileService.doesObjectExist(ossEntry);
        return  R.ok(""+b);
    }





}
