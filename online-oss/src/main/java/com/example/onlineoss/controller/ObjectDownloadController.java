package com.example.onlineoss.controller;

import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ObjectDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件下载
 */
@RequestMapping("/objectDownloadController")
@RestController
public class ObjectDownloadController {


    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKeyId;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessKeySecret ;

    @Value("${spring.cloud.alicloud.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucketName;

    @Autowired
    private ObjectDownloadService objectDownloadService;

    /**
     *  文件下载-流式下载
     * @param bucketName 储存对象
     * @param objectName 文件对象
     * @return
     */
    @RequestMapping("/downLoadFile")
    public R downLoadFile(String bucketName, String objectName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        ossEntry.setObjectName(objectName);
        objectDownloadService.downLoadFile(ossEntry);
        return  R.ok();
    }

    /**
     *  文件下载-下载到本地文件
     * @param bucketName 储存对象
     * @param objectName 文件对象
     * @return
     */
    @RequestMapping("/downLoadFileLoca")
    public R downLoadFileLoca(String bucketName, String objectName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        ossEntry.setObjectName(objectName);
        objectDownloadService.downLoadFileLoca(ossEntry);
        return  R.ok();
    }

    /**
     *  文件下载-指定正常的下载范围
     * @param bucketName 储存对象
     * @param objectName 文件对象
     * @return
     */
    @RequestMapping("/downLoadFilerange")
    public R downLoadFilerange(String bucketName, String objectName){
        OssEntry ossEntry = new OssEntry(accessKeyId, accessKeySecret, endpoint, bucketName);
        ossEntry.setObjectName(objectName);
        objectDownloadService.downLoadFilerange(ossEntry);
        return  R.ok();
    }


}
