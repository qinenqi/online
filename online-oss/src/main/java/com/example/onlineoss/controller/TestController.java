package com.example.onlineoss.controller;

import com.example.onlinecommon.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testController")
@Slf4j
public class TestController {


    /**
     *  oss 测试 R
     * @return
     */
    @RequestMapping("/test")
    public R test(){
        log.info("oss 测试 R 成功");
        return R.ok("oss 测试 R 成功");
    }
}
