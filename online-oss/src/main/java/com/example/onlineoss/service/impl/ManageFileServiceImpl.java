package com.example.onlineoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.Bucket;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ManageFileService;
import com.example.onlineoss.utils.OssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ManageFileServiceImpl implements ManageFileService {



    /**
     * 判断文件是否存在
     * @param ossEntry
     * @return
     */
    public boolean doesObjectExist(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        String objectName = ossEntry.getObjectName();
        // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
        boolean b = oss.doesObjectExist(bucketName, objectName);
        log.info("文件是否存在：" + b);
        ossUtil.shutdown(oss);
        return b;
    }


}
