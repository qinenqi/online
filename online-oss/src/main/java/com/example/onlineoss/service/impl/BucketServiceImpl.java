package com.example.onlineoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.*;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.BucketService;
import com.example.onlineoss.utils.OssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class BucketServiceImpl implements BucketService {


    /**
     * 创建存储空间
     *  存储空间类型
     *  数据容灾类型
     *  存储空间的权限
     * @param ossEntry
     */
    public void createBucket(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        // 创建CreateBucketRequest 对象
        CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
        // 设置储存空间的 存储空间类型为标准存储
        createBucketRequest.setStorageClass(StorageClass.Standard);
        // 数据容灾类型设置为本地冗余存储
        createBucketRequest.setDataRedundancyType(DataRedundancyType.LRS);
        // 设置存储空间的权限为 公共读 默认为私有
        createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
        // 创建存储空间
        oss.createBucket(createBucketRequest);

        ossUtil.shutdown(oss);
    }



    /**
     * 列举存储空间
     */
    public void listBucket(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        List<Bucket> buckets = oss.listBuckets();
        for(Bucket bucket: buckets){
            log.info("存储空间的名称为：" + bucket.getName());
        }
        ossUtil.shutdown(oss);
    }

    /**
     *  判断存储空间是否存在
     * @param ossEntry
     * @return
     */
    public boolean doesBucketExist(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        boolean b = oss.doesBucketExist(bucketName);
        ossUtil.shutdown(oss);
        return b;
    }

    /**
     * 获取存储空间所在的地域
     * @param ossEntry
     * @return
     */
    public String getBucketLocation(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        String bucketLocation = oss.getBucketLocation(bucketName);
        ossUtil.shutdown(oss);
        return bucketLocation;
    }

    /**
     * 获取存储空间所在的地域
     * @param ossEntry
     */
    public BucketInfo getBucketInfo(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        BucketInfo bucketInfo = oss.getBucketInfo(bucketName);
        // 获取地域
        String location = bucketInfo.getBucket().getLocation();
        log.info("获取地域：" + location);

        // 获取创建日期
        Date creationDate = bucketInfo.getBucket().getCreationDate();
        log.info("获取创建日期:" + creationDate);

        // 获取拥有者信息
        Owner owner = bucketInfo.getBucket().getOwner();
        log.info("获取拥有者信息:" + owner);

        // 获取权限信息
        Set<Grant> grants = bucketInfo.getGrants();
        log.info("获取权限信息：" + grants);

        // 获取容灾类型
        DataRedundancyType dataRedundancyType = bucketInfo.getDataRedundancyType();
        log.info("获取容灾类型:" + dataRedundancyType);
        ossUtil.shutdown(oss);
        return bucketInfo;
    }

    /**
     * 设置存储空间的访问权限
     * @param ossEntry
     */
    public void setBucketAcl(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        // 设置存储空间的访问权限为私有。
        oss.setBucketAcl(bucketName, CannedAccessControlList.Private);
        ossUtil.shutdown(oss);
    }

    /**
     * 获取存储空间的访问权限
     * @param ossEntry
     * @return
     */
    public AccessControlList getBucketAcl(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        // 获取存储空间的访问权限
        AccessControlList bucketAcl = oss.getBucketAcl(bucketName);
        ossUtil.shutdown(oss);
        return bucketAcl;
    }

    /**
     * 删除存储空间
     * @param ossEntry
     */
    public void deleteBucket(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        oss.deleteBucket(bucketName);
        ossUtil.shutdown(oss);
    }

    /**
     * 设置Bucket标签
     * @param ossEntry
     */
    public void setBucketTagging(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        SetBucketTaggingRequest setBucketTaggingRequest = new SetBucketTaggingRequest(bucketName);
        setBucketTaggingRequest.setTag("test01", "开始测试 设置Bucket标签 的功能");
        setBucketTaggingRequest.setTag("test02", "结束测试 设置Bucket标签 的功能");
        oss.setBucketTagging(setBucketTaggingRequest);
        ossUtil.shutdown(oss);
    }


    /**
     * 获取Bucket标签信息
     * @param ossEntry
     */
    public Map<String, String> getAllTags(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        TagSet bucketTagging = oss.getBucketTagging(bucketName);
        Map<String, String> allTags = bucketTagging.getAllTags();
        for(Map.Entry<String, String> map: allTags.entrySet()){
            String key = map.getKey();
            String value = map.getValue();
            log.info("key:" + key + " value:" + value);
        }
        ossUtil.shutdown(oss);
        return allTags;
    }

    /**
     * 列举带指定标签的Bucket
     * @param ossEntry
     */
    public void listBuckets(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        // 列举带指定标签的Bucket。
        ListBucketsRequest listBucketsRequest = new ListBucketsRequest();
        listBucketsRequest.setTag("test01", "开始测试 设置Bucket标签 的功能");
        BucketList bucketList = oss.listBuckets(listBucketsRequest);
        for (Bucket o : bucketList.getBucketList()) {
            System.out.println("list result bucket: " + o.getName());
        }
        ossUtil.shutdown(oss);
    }

    /**
     * 删除Bucket标签
     * @param ossEntry
     */
    public void deleteBucketTagging(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        oss.deleteBucketTagging(bucketName);
        ossUtil.shutdown(oss);
    }
}
