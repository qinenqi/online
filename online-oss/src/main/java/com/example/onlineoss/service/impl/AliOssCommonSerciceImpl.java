package com.example.onlineoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.AliOssCommonSercice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.*;
import java.util.List;

/**
 *  阿里云 对象储存的 公共方法
 */
@Service
@Slf4j
public class AliOssCommonSerciceImpl implements AliOssCommonSercice {

    /**
     *  初始化 OSS 对象
     * @param ossEntry
     * @return
     */
    public OSS init(OssEntry ossEntry){
        String accessKeyId = ossEntry.getAccessKeyId();
        String accessKeySecret = ossEntry.getAccessKeySecret();
        String endpoint = ossEntry.getEndpoint();
        // 创建 OSSClient 实例
        OSS build = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        return build;
    }

    /**
     *  关闭 oss
     * @param build
     */
    public void shutdown(OSS build){
        build.shutdown();
    }

    /**
     *  创建 存储空间
     * @param ossEntry
     * @return
     */
    public R createBucket(OssEntry ossEntry){
        String bucketName = ossEntry.getBucketName();
        OSS build = init(ossEntry);

        // 创建存储空间
        build.createBucket(bucketName);
        shutdown(build);
        log.info("创建成功");
        return R.ok("success");
    }

    /**
     *  上传文件
     * @param
     * @return
     */
    public R uploadFile(OssEntry ossEntry){
        String bucketName = ossEntry.getBucketName();
        OSS build = init(ossEntry);
        String testStr = "我准备上传文件";
        String objectName = "要上传的文件名.txt";
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(objectName.getBytes());
        build.putObject(bucketName, objectName, byteArrayInputStream);
        shutdown(build);
        return R.ok("success");
    }


    /**
     * 下载文件
     * @param fileName
     * @return
     */
    public R downFile(String fileName, OssEntry ossEntry){
        String bucketName = ossEntry.getBucketName();
        OSS build = init(ossEntry);
        // 调用ossClient.getObject返回一个OSSObject实例，该实例包含文件内容及文件元信息。
        OSSObject object = build.getObject(bucketName, fileName);
        // 调用ossObject.getObjectContent获取文件输入流，可读取此输入流获取其内容。
        InputStream objectContent = object.getObjectContent();
        try{
            if (objectContent != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(objectContent));
                while (true) {
                    String line = reader.readLine();
                    if (line == null) break;
                    System.out.println("\n" + line);
                }
                // 数据读取完成后，获取的流必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
                objectContent.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        shutdown(build);
        return R.ok("success");
    }


    /**
     *  列举文件
     * @return
     */
    public R listObject(OssEntry ossEntry){
        String bucketName = ossEntry.getBucketName();
        OSS build = init(ossEntry);
        ObjectListing objectListing = build.listObjects(bucketName);
        List<OSSObjectSummary> objectSummaries = objectListing.getObjectSummaries();
        for(OSSObjectSummary oSSObjectSummary: objectSummaries){
            log.info(" _ " + oSSObjectSummary.getKey() + " size = " + oSSObjectSummary.getSize());
        }
        shutdown(build);
        return R.ok("success");
    }

    /**
     *  删除文件
     * @param ossEntry
     * @return
     */
    public R deleteFile(OssEntry ossEntry){
        String bucketName = ossEntry.getBucketName();
        String fileNmae = ossEntry.getFileNmae();
        OSS build = init(ossEntry);
        build.deleteObject(bucketName, fileNmae);

        shutdown(build);
        return R.ok("success");
    }
}
