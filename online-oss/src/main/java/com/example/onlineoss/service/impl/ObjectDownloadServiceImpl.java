package com.example.onlineoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectRequest;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ObjectDownloadService;
import com.example.onlineoss.utils.OssUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Buffer;
import org.springframework.stereotype.Service;
import sun.security.util.Length;

import java.io.*;

@Slf4j
@Service
public class ObjectDownloadServiceImpl implements ObjectDownloadService {



    /**
     * 文件下载
     * @param ossEntry
     */
    public void downLoadFile(OssEntry ossEntry){
        try{
            OssUtil ossUtil = new OssUtil();
            OSS oss = ossUtil.init(ossEntry);
            String bucketName = ossEntry.getBucketName();
            String objectName = ossEntry.getObjectName();
            // ossObject 包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流
            OSSObject object = oss.getObject(bucketName, objectName);
            // 读取文件内容
            InputStream objectContent = object.getObjectContent();
            InputStreamReader inputStreamReader = new InputStreamReader(objectContent);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while(true){
                String line = bufferedReader.readLine();
                if(line  == null){
                    break;
                }
                log.info("读取的内容：" + line);
            }
            bufferedReader.close(); // 数据读取完成后，获取的流必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
            ossUtil.shutdown(oss);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件下载-下载到本地文件
     * @param ossEntry
     */
    public void downLoadFileLoca(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        String objectName = ossEntry.getObjectName();
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, objectName);
        File file = new File("E:\\examplefile.txt");
        oss.getObject(getObjectRequest, file);
        ossUtil.shutdown(oss);
    }

    /**
     * 指定正常的下载范围
     * @param ossEntry
     */
    public void downLoadFilerange(OssEntry ossEntry){
        try{
            OssUtil ossUtil = new OssUtil();
            OSS oss = ossUtil.init(ossEntry);
            String bucketName = ossEntry.getBucketName();
            String objectName = ossEntry.getObjectName();
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, objectName);
            getObjectRequest.setRange(0, 100);
            OSSObject object = oss.getObject(getObjectRequest);
            byte[] buf = new byte[1024];
            InputStream objectContent = object.getObjectContent();
            InputStreamReader inputStreamReader = new InputStreamReader(objectContent);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while(true){
                String s = bufferedReader.readLine();
                if(s == null){
                    break;
                }
                log.info("读取的内容：" + s);
            }
            objectContent.close();
            ossUtil.shutdown(oss);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}



















