package com.example.onlineoss.service;

import com.example.onlineoss.entry.OssEntry;

public interface ManageFileService {

    /**
     * 判断文件是否存在
     * @param ossEntry
     * @return
     */
    public boolean doesObjectExist(OssEntry ossEntry);
}
