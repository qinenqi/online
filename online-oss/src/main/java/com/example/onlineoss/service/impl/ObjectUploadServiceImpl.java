package com.example.onlineoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.PutObjectRequest;
import com.example.onlineoss.entry.OssEntry;
import com.example.onlineoss.service.ObjectUploadService;
import com.example.onlineoss.utils.OssUtil;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;


@Service
public class ObjectUploadServiceImpl implements ObjectUploadService {


    /**
     * 上传字符串
     * @param ossEntry
     */
    public void putObject(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        String str = "hello oss";
        // 创建  PutObjectRequest 对象，
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        // bucketname 上传的路径 字节流
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, "测试上传上传字符串.txt", byteArrayInputStream);
        oss.putObject(putObjectRequest);
        ossUtil.shutdown(oss);
    }


    /**
     * 上传Byte数组
     * @param ossEntry
     */
    public void putObjectByte(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();
        byte[] bytes = "hello oss".getBytes();
        // 创建  PutObjectRequest 对象，
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        // bucketname 上传的路径 字节流
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, "测试上传Byte数组.txt", byteArrayInputStream);
        oss.putObject(putObjectRequest);
        ossUtil.shutdown(oss);
    }

    /**
     * 上传网络流
     * @param ossEntry
     */
    public void putObjectUrl(OssEntry ossEntry){
        try{
            OssUtil ossUtil = new OssUtil();
            OSS oss = ossUtil.init(ossEntry);
            String bucketName = ossEntry.getBucketName();
            // 填写网络流地址
            InputStream inputStream = new URL("https://www.aliyun.com/").openStream();
            oss.putObject(bucketName, "测试上传网络流.txt", inputStream);
            ossUtil.shutdown(oss);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件流
     * @param ossEntry
     */
    public void putObjectFile(OssEntry ossEntry){
        try{
            OssUtil ossUtil = new OssUtil();
            OSS oss = ossUtil.init(ossEntry);
            String bucketName = ossEntry.getBucketName();
            FileInputStream fileInputStream = new FileInputStream("E:\\生活\\租房文档.txt");
            oss.putObject(bucketName, "测试上传文件流.txt", fileInputStream);
            ossUtil.shutdown(oss);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * 文件上传
     * @param ossEntry
     */
    public void putObjectFile02(OssEntry ossEntry){
        OssUtil ossUtil = new OssUtil();
        OSS oss = ossUtil.init(ossEntry);
        String bucketName = ossEntry.getBucketName();

        File file = new File("E:\\生活\\租房文档.txt");
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, "测试上传文件02.txt", file);
        oss.putObject(putObjectRequest);
        ossUtil.shutdown(oss);
    }

}
