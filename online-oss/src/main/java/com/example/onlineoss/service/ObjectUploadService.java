package com.example.onlineoss.service;

import com.example.onlineoss.entry.OssEntry;

public interface ObjectUploadService {

    /**
     * 上传字符串
     * @param ossEntry
     */
    public void putObject(OssEntry ossEntry);

    /**
     * 上传Byte数组
     * @param ossEntry
     */
    public void putObjectByte(OssEntry ossEntry);

    /**
     * 上传网络流
     * @param ossEntry
     */
    public void putObjectUrl(OssEntry ossEntry);

    /**
     * 上传文件流
     * @param ossEntry
     */
    public void putObjectFile(OssEntry ossEntry);

    /**
     * 文件上传
     * @param ossEntry
     */
    public void putObjectFile02(OssEntry ossEntry);
}
