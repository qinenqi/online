package com.example.onlineoss.service;

import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;

public interface AliOssCommonSercice {

    /**
     *  创建 存储空间
     * @param ossEntry
     * @return
     */
    public R createBucket(OssEntry ossEntry);


    /**
     *  上传文件
     * @param
     * @return
     */
    public R uploadFile(OssEntry ossEntry);

    /**
     * 下载文件
     * @param fileName
     * @return
     */
    public R downFile(String fileName, OssEntry ossEntry);

    /**
     *  创建 对象存储的 存储空间
     * @return
     */
    public R listObject(OssEntry ossEntry);

    /**
     *  删除文件
     * @param ossEntry
     * @return
     */
    public R deleteFile(OssEntry ossEntry);
}
