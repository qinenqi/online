package com.example.onlineoss.service;

import com.example.onlineoss.entry.OssEntry;

public interface ObjectDownloadService {

    /**
     * 文件下载
     * @param ossEntry
     */
    public void downLoadFile(OssEntry ossEntry);

    /**
     * 文件下载-下载到本地文件
     * @param ossEntry
     */
    public void downLoadFileLoca(OssEntry ossEntry);

    /**
     * 指定正常的下载范围
     * @param ossEntry
     */
    public void downLoadFilerange(OssEntry ossEntry);

}
