package com.example.onlineoss.service;

import com.aliyun.oss.model.AccessControlList;
import com.aliyun.oss.model.BucketInfo;
import com.example.onlinecommon.utils.R;
import com.example.onlineoss.entry.OssEntry;

import java.util.Map;

public interface BucketService {


    /**
     * 创建存储空间
     * @param ossEntry
     */
    public void createBucket(OssEntry ossEntry);

    /**
     * 列举存储空间
     */
    public void listBucket(OssEntry ossEntry);

    /**
     *  判断存储空间是否存在
     * @param ossEntry
     * @return
     */
    public boolean doesBucketExist(OssEntry ossEntry);

    /**
     * 获取存储空间所在的地域
     * @param ossEntry
     * @return
     */
    public String getBucketLocation(OssEntry ossEntry);

    /**
     * 获取存储空间所在的地域
     * @param ossEntry
     */
    public BucketInfo getBucketInfo(OssEntry ossEntry);

    /**
     * 设置存储空间的访问权限
     * @param ossEntry
     */
    public void setBucketAcl(OssEntry ossEntry);

    /**
     * 获取存储空间的访问权限
     * @param ossEntry
     * @return
     */
    public AccessControlList getBucketAcl(OssEntry ossEntry);

    /**
     * 删除存储空间
     * @param ossEntry
     */
    public void deleteBucket(OssEntry ossEntry);

    /**
     * 设置Bucket标签
     * @param ossEntry
     */
    public void setBucketTagging(OssEntry ossEntry);

    /**
     * 获取Bucket标签信息
     * @param ossEntry
     */
    public Map<String, String> getAllTags(OssEntry ossEntry);

    /**
     * 列举带指定标签的Bucket
     * @param ossEntry
     */
    public void listBuckets(OssEntry ossEntry);

    /**
     * 删除Bucket标签
     * @param ossEntry
     */
    public void deleteBucketTagging(OssEntry ossEntry);
}
