package com.example.onlineoss.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.example.onlineoss.entry.OssEntry;

/**
 *  oss 对象储存的工农方法类
 */
public class OssUtil {


    /**
     *  初始化 OSS 对象
     * @param ossEntry
     * @return
     */
    public OSS init(OssEntry ossEntry){
        String accessKeyId = ossEntry.getAccessKeyId();
        String accessKeySecret = ossEntry.getAccessKeySecret();
        String endpoint = ossEntry.getEndpoint();
        // 创建 OSSClient 实例
        OSS build = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        return build;
    }

    /**
     *  关闭 oss
     * @param build
     */
    public void shutdown(OSS build){
        build.shutdown();
    }
}
