package com.example.onlineoss.entry;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 *  oss  对象实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class OssEntry implements Serializable {

    /**
     *  密钥 标识用户
     */
    private String accessKeyId;

    /**
     *  密钥 验证用户的密钥
     */
    private String accessKeySecret ;

    /**
     * 访问域名
     */
    private String endpoint;

    /**
     * 存储空间 名字
     */
    private String bucketName;

    /**
     *  文件名
     */
    private String fileNmae;

    /**
     *  文件对象名
     */
    private String objectName;

    public OssEntry(String accessKeyId, String accessKeySecret, String endpoint, String bucketName) {
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.endpoint = endpoint;
        this.bucketName = bucketName;
    }

    public OssEntry(String accessKeyId, String accessKeySecret, String endpoint, String bucketName, String fileNmae) {
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.endpoint = endpoint;
        this.bucketName = bucketName;
        this.fileNmae = fileNmae;
    }
}
