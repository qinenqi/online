package com.example.onlineoss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


// 如果想连接数据库，请更换为  @SpringBootApplication
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class OnlineOssApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineOssApplication.class, args);
    }

}
