package com.example.onlineproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication
@EnableFeignClients(basePackages = "com.example.onlineproject.feign")
public class OnlineProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineProjectApplication.class, args);
    }

}
