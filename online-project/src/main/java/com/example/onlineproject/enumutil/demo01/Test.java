package com.example.onlineproject.enumutil.demo01;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Test test  = new Test();
        test.test01();
        log.info("--------------------------------");
        test.test02();

    }

    /**
     *  获取枚举类的常量
     */
    public void test01(){
        Color c1 = Color.RED;
        log.info("得到的常量是：" + c1);
    }

    /**
     *  Color.values()
     *  color.ordinal()
     *  Color.valueOf("RED")
     */
    public void test02(){
        Color[] values = Color.values();  // 获取枚举类的数组  values() 返回枚举类中所有的值。
        for(Color color: values){
            log.info("得到的每个常量为：" + color);
            int ordinal = color.ordinal();  // 得到每个对象的索引值
            log.info("得到每个对象的索引值:" + ordinal);
        }
        Color color = Color.valueOf("RED");
        log.info("Color.valueOf(\"RED\")：" +color);
    }
}








