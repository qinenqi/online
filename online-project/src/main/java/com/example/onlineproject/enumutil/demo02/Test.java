package com.example.onlineproject.enumutil.demo02;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {
    public static void main(String[] args) {
        Test test = new Test();
        test.test01();
        test.test02();
        test.test03();
    }

    /**
     *  获取 索引为 1 的名称
     */
    public void test01(){
        String name = Color.getName(1);
        log.info("名称为：" + name);
    }

    /**
     *  获取该对象的索引和名称
     */
    public void test02(){
        Color red = Color.RED;
        int index = red.getIndex();
        String name = red.getName();
        log.info("获取该对象的索引: " + index + ",获取该对象的名称： " + name );
    }


    /**
     * 测试 toString 方法
     */
    public void test03(){
        Color red = Color.RED;
        log.info("测试 toString 方法：" + red);
    }
}
