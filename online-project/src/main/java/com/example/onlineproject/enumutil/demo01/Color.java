package com.example.onlineproject.enumutil.demo01;

/**
 * Java 枚举是一个特殊的类，一般表示一组常量，比如一年的 4 个季节，一个年的 12 个月份，一个星期的 7 天，方向有东南西北等。
 * Java 枚举类使用 enum 关键字来定义，各个常量使用逗号 , 来分割
 *  定义一个颜色的枚举类   定义常量
 *  枚举类 Color 颜色常量有 RED, GREEN, BLUE，分别表示红色，绿色，蓝色
 */
public enum Color {
    RED, GREEN, BLUE;
}
