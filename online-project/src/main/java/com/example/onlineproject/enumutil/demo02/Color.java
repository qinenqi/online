package com.example.onlineproject.enumutil.demo02;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  枚举类
 */
public enum Color {
    RED("红色", 1), WHITE("白色", 2), GREEN("绿色", 3);

    private String name;
    private int index;



    private Color(String name, int index){
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     *  根据索引获取名称
     * @param index
     * @return
     */
    public static String getName(int index){
        for(Color c: Color.values()){
            if(c.getIndex() == index){
                return c.getName();
            }
        }
        return null;
    }

    /**
     *  根据索引 获取 枚举类
     */
    public static Color of(int index){
        Color[] values = Color.values();
        for(Color color: values){
            int index1 = color.getIndex();
            if(index1 == index){
                return color;
            }
        }
        return null;
    }

    /**
     *  将枚举类 转化为 List 集合
     * @return
     */
    public static List toList(){
        List<Map<String, Object>>  lists = new ArrayList<Map<String, Object>>();
        Color[] values = Color.values();
        for(Color color: values){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("index", color.getIndex());
            map.put("name", color.getName());
            lists.add(map);
        }
        return lists;
    }

    /**
     *  覆盖 toString  方法
     * @return
     */
    @Override
    public String toString(){
        return this.index + "_" + this.name;
    }

}
