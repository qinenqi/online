package com.example.onlineproject.config;

import org.springframework.amqp.core.*;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
public class RabbitmqConfig {

    /**
     *  配置一个工作模型队列
     * @return
     */
    @Bean
    public Queue queueWork1() {
        return new Queue("queue_work");
    }


    /**
     *  发布订阅模式
     *  声明两个队列
     * @return
     */
    @Bean
    public Queue queueFanout1() {
        return new Queue("queue_fanout1");
    }
    @Bean
    public Queue queueFanout2() {
        return new Queue("queue_fanout2");
    }
    // 准备一个交换机
    @Bean
    public FanoutExchange exchangeFanout() {
        return new FanoutExchange("exchange_fanout");
    }

    /**
     *  将队列绑定到交换机上
     * @return
     */
    @Bean
    public Binding bindingExchange1() {
        Queue queue = queueFanout1();
        return BindingBuilder.bind(queue).to(exchangeFanout());
    }

    @Bean
    public Binding bindingExchange2() {
        Queue queue = queueFanout2();
        return BindingBuilder.bind(queue).to(exchangeFanout());
    }


    /**
     *  topic 模型
     * @return
     */
    @Bean
    public Queue queueTopic1() {
        return new Queue("queue_topic1");
    }
    @Bean
    public Queue queueTopic2() {
        return new Queue("queue_topic2");
    }
    @Bean
    public TopicExchange exchangeTopic() {
        return new TopicExchange("exchange_topic");
    }
    @Bean
    public Binding bindingTopic1() {
        Queue queue = queueTopic1();
        return BindingBuilder.bind(queue).to(exchangeTopic()).with("topic.#");
    }
    @Bean
    public Binding bindingTopic2() {
        Queue queue = queueTopic2();
        return BindingBuilder.bind(queue).to(exchangeTopic()).with("topic.*");
    }



    /**
     *  confirm机制
     *  测试confirm 机制，专门创建了一个队列
     * @return
     */
    @Bean
    public Queue queueConfirm() {
        return new Queue("queue_confirm");
    }



    /**
     *  return机制
     * @return
     */
    @Bean
    public Queue queueReturn() {
        return new Queue("queue_return");
    }
    @Bean
    public TopicExchange exchangeReturn() {
        return new TopicExchange("exchange_return");
    }
    @Bean
    public Binding bindingReturn() {
        Queue queue = queueReturn();
        return BindingBuilder.bind(queue).to(exchangeReturn()).with("return.*");
    }

}
