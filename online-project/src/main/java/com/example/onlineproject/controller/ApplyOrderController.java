package com.example.onlineproject.controller;

import com.example.onlinecommon.utils.R;
import com.example.onlineproject.entry.ApplyOrder;
import com.example.onlineproject.service.ApplyOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/applyController")
@Slf4j
public class ApplyOrderController {

    @Autowired
    private ApplyOrderService applyOrderService;

    /**
     *  根据订单 id 获取订单信息和支付二维码信息
     * @param id
     * @return
     */
    @RequestMapping("/getApplyOrderCode")
    public R getApplyOrderCode(@RequestParam Long id){
        ApplyOrder applyOrderCode = applyOrderService.getApplyOrderCode(id);
        return R.ok(applyOrderCode);
    }

    /**
     * 阿里支付宝 回调
     * @param request
     * @return
     */
    @PostMapping("/notify")
    public String alipayNotify(HttpServletRequest request){
        boolean signVerified = applyOrderService.checkSign(request.getParameterMap());

        if(signVerified){//验证成功  total_amount
            System.out.println("验证成功,去更新状态");
            String out_trade_no = new String(request.getParameter("out_trade_no")); //商户订单号
            String trade_no = new String(request.getParameter("trade_no"));//支付宝交易号
            String trade_status = new String(request.getParameter("trade_status"));//交易状态
            String price = new String(request.getParameter("total_amount"));//交易金额
            log.info("交易状态——trade_status:" + trade_status);

            //判断该笔订单是否在商户网站中已经做过处理
            //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
            //如果有做过处理，不执行商户的业务程序
            if(trade_status.equals("TRADE_FINISHED")){
                //交易结束，不可退款
                //注意：退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
                //可执行修改订单状态等
                return ("finished");
            }else if (trade_status.equals("TRADE_SUCCESS")){
                log.info("交易支付成功");
                //交易支付成功
                //注意：付款完成后，支付宝系统发送该交易状态通知
                //可执行修改订单状态等
//                return ("success");
                return ("false");
            }else if (trade_status.equals("WAIT_BUYER_PAY")) {
                //交易创建，等待买家付款
                //可执行修改订单状态等
                return ("wait");
            }else{
                //未付款交易超时关闭，或支付完成后全额退款(交易失败)
                //可执行修改订单状态等
                return ("close");
            }
        }else{
            System.out.println("验证失败,不去更新状态");
            //可执行修改订单状态等
            return ("fail");
        }

    }



}
