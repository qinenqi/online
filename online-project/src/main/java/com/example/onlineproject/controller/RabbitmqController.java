package com.example.onlineproject.controller;

import com.example.onlineproject.service.RabbitmqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rabbitmqController")
public class RabbitmqController {
    @Autowired
    private RabbitmqService rabbitmqService;

    /**
     * work模型
     * @return
     */
    @RequestMapping("/sendWork")
    public Object sendWork() {
        rabbitmqService.sendWork();
        return "发送成功...";
    }

    /**
     *  发布订阅模型
     * @return
     */
    @RequestMapping("/sendPublish")
    public String sendPublish() {
        rabbitmqService.sendPublish();
        return "发送成功...";
    }

    /**
     *  topic模型
     * @return
     */
    @RequestMapping("/sendTopic")
    public String sendTopic() {
        rabbitmqService.sendTopic();
        return "发送成功...";
    }

    /**
     *  confirm机制
     * @return
     */
    @RequestMapping("/sendConfirm")
    public String sendConfirm() {
        rabbitmqService.sendConfirm();
        return "发送成功...";
    }


    /**
     *  return机制
     * @return
     */
    @RequestMapping("/sendReturn")
    public String sendReturn() {
        rabbitmqService.sendReturn();
        return "发送成功...";
    }



}
