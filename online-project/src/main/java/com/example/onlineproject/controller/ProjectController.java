package com.example.onlineproject.controller;


import com.example.onlinecommon.utils.R;
import com.example.onlinecommon.entry.PageUtil;
import com.example.onlineproject.entry.Project;
import com.example.onlineproject.service.ProjectService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品 控制器
 *  用来验证 分页组件的
 */
@RestController
@RequestMapping("/projectController")
@Slf4j
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    /**
     *  批量新增数据
     * @return
     */
    @RequestMapping("/insertBatchProject")
    public R insertBatchProject(){
       projectService.insertBatchProject();
       return R.ok();
    }

    /**
     *  查询所有的数据
     * @return
     */
    @RequestMapping("/selectProjectList")
    public R selectProjectList(@RequestBody PageUtil pageUtil){
        PageInfo<Project> projectPageInfo = projectService.selectProjectList(pageUtil);
        return R.ok(projectPageInfo);
    }

    /**
     *  根据 id 更新数据
     * @param project
     * @return
     */
    @PostMapping("/updateProjectById")
    public R updateProjectById(@RequestBody Project project){
        projectService.updateProjectById(project);
        return R.ok();
    }

}
