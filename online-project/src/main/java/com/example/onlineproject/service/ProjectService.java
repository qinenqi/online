package com.example.onlineproject.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.onlinecommon.entry.PageUtil;
import com.example.onlineproject.entry.Project;
import com.github.pagehelper.PageInfo;


public interface ProjectService extends IService<Project> {

    /**
     * 批量新增数据
     */
    public void  insertBatchProject();

    /**
     * 查询所有的数据
     * @return
     */
    public PageInfo<Project> selectProjectList(PageUtil pageUtil);

    /**
     *  根据 id 更新数据
     * @param project
     */
    public void updateProjectById(Project project);
}
