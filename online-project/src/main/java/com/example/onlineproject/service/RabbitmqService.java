package com.example.onlineproject.service;


public interface RabbitmqService {

    public void sendWork();

    public void sendPublish();

    public void sendTopic();

    public void sendConfirm();

    public void sendReturn();
}
