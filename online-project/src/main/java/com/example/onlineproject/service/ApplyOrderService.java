package com.example.onlineproject.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.onlineproject.entry.ApplyOrder;
import java.util.Map;

public interface ApplyOrderService extends IService<ApplyOrder> {

    /**
     *  根据订单id 获取该订单的支付信息
     * @param id
     * @return
     */
    public ApplyOrder getApplyOrderCode(Long id);

    /**
     * 回调方法必须调用的验签方法，在回调结果前调用，确保是由支付宝发起的请求
     * @return boolean
     */
    public  boolean checkSign(Map<String,String[]> requestParams);
}
