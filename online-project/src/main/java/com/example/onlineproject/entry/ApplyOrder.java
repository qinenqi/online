package com.example.onlineproject.entry;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Slf4j
public class ApplyOrder implements Serializable {

    private Long id;

    /**
     * 订单编号  order_no
     */
    private String orderNo;

    /**
     *  订单价格  price
     */
    private String price;

    /**
     *  订单状态  1000:未支付 2000：已支付 3000：支付失败 4000：默认
     */
    private int status;

    /**
     * 订单内容  订单内容
     */
    private String content;

    /**
     *  商家信息  shop_name
     */
    private String shopName;

    /**
     * 订单描述  description
     */
    private String description;

    /**
     * 创建时间  create_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间  update_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     *  备注  remark
     */
    private String remark;

    /**
     *  二维码url  code_url
     */
    private String codeUrl;


}
