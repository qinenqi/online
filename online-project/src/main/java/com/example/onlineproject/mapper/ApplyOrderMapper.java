package com.example.onlineproject.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.onlineproject.entry.ApplyOrder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ApplyOrderMapper extends BaseMapper<ApplyOrder> {
}
