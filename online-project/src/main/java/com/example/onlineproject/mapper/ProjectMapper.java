package com.example.onlineproject.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.onlineproject.entry.Project;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@Mapper
public interface ProjectMapper extends BaseMapper<Project> {


    /**
     * 查询所有的数据
     * @return
     */
    public List<Project> selectProjectList();
}
