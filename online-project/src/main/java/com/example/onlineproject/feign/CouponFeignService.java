package com.example.onlineproject.feign;


import com.example.onlinecommon.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("online-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/couponController/testSeata")
    R testSeata();

}
