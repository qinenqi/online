package com.example.onlinessoclient.service.impl;

import com.example.onlinessoclient.service.HelloService;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
public class HelloServiceImpl implements HelloService {

    @Autowired
    private CloseableHttpClient httpClient;

    @Autowired
    private RequestConfig config;

    public String doGet(String url){
        String s = null;
        try{
            HttpGet httpGet = new HttpGet(url);   // 声明 http get 请求
            httpGet.setConfig(config);   // 装载配置信息

            CloseableHttpResponse response = this.httpClient.execute(httpGet);  // 发起请求
            s = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

}
