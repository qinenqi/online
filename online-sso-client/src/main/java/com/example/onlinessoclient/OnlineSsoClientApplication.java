package com.example.onlinessoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineSsoClientApplication.class, args);
    }

}
