package com.example.onlinessoclient.controller;

import com.example.onlinessoclient.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


/**
 *  单点登录值之thymeleaf渲染页面
 */
@Controller
@RequestMapping("/helloController")
@Slf4j
public class HelloController {


    /**
     *  登录服务器 登录地址
     */
    @Value("${sso.server.url}")
    private String ssoServerUrl;

    @Value("${sso.server.baseurl}")
    private String ssoServerBaseUrl;

    /**
     *  本客户端地址
     */
    @Value("${sso.client01}")
    private String ssoClient01;

    @Autowired
    private HelloService helloService;


    /**
     * 无需登录就可访问
     * @return
     */
    @ResponseBody
    @GetMapping("/hello")
    public String hello(){
        return "这个是不受保护的资源";
    }


    /**
     * 感知这次是在 ssoserver 登录成功跳回来的。
     *  @RequestParam(required = false) String token  第一次访问的时候，肯定没有携带token,因此是非必传参数
     * @return
     */
    @GetMapping("/employees")
    public String employees(Model model, HttpSession session, @RequestParam(required = false) String token ){

        if(!StringUtils.isEmpty(token)){  // 没有携带 token，则需要跳转至登录服务器
            // 去ssoserver获取当前token真正对应的用户信息
            String url = ssoServerBaseUrl + "/loginController/userInfo?token=" + token;
            String s = helloService.doGet(url);
            log.info("校验信息：" + s);
            if(s.equals("true")){  // 身份信息验证成功
                session.setAttribute("loginUser","");
            }else{

            }
        }
        // 从当前会话获取 loginUser
        Object loginUser = session.getAttribute("loginUser");
        if(loginUser == null){  // 为空，说明 会话中没有 loginUser 的信息
            // 没有登录 则需要跳转至登录服务器进行登录
            String returnUrl = ssoClient01 + "/helloController/employees";
            return "redirect:" + ssoServerUrl +"?redirect_url=" + returnUrl;
        }else{
            List<String> emps = new ArrayList<>();
            emps.add("张三");
            emps.add("李四");

            model.addAttribute("emps",emps);
            return "list";
        }
    }



}
