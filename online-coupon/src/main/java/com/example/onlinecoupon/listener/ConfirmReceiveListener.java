package com.example.onlinecoupon.listener;


import com.example.onlinecommon.entry.User;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ConfirmReceiveListener {

    @RabbitListener(queues = "queue_confirm")
    public void receiveMsg(User user) {
        System.out.println("接收到的消息为：" + user);
    }
}
