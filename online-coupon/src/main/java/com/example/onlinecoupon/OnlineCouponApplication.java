package com.example.onlinecoupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching  //开启注解式缓存
public class OnlineCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineCouponApplication.class, args);
    }

}
