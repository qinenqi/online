package com.example.gulimailtest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testKController")
public class TestKController {


    /**
     *  http://127.0.0.1:30020/testKController/test
     * @return
     */
    @RequestMapping("/test")
    public String test(){
        return "gulimail-test 测试成功";
    }
}
